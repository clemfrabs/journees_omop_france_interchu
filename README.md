# L'utopie

#### Partageons

La solidarité, le partage et l’entraide entre les différents acteurs sont les valeurs centrales en santé. Au même titre qu’Internet est un bien commun, le savoir en informatique médicale doit être disponible et accessible à tous. Nous voulons donc promouvoir la dimension éthique particulière qu’engendre l’ouverture de l’innovation dans le domaine médical et nous voulons prendre des mesures actives pour empêcher la privatisation de la médecine.

#### Interopérons

L’interopérabilité des systèmes informatisés est le moteur du partage des connaissances et des compétences ainsi que le moyen de lutter contre l’emprisonnement technologique. En santé, l’interopérabilité est le gage de la reproductibilité de la recherche, du partage et de la comparaison des pratiques pour une recherche performante et transparente. Il ne peut pas y avoir d’interopérabilité sans communauté.

# Présentation générale

Les journées OMOP s'adressent préférentiellement aux personnes francophones (ingénieur.e.s du secteur public, ...) utilisant le modèle OMOP ou voulant l'utiliser dans les prochains mois.

Au sein des réunions OMOP France, nous voulons :
1. Faire des points d'étape des diverses transformations réalisées dans les centres hospitaliers. Nous avons du code et de l'expérience à partager !
2. Faire un état des lieux des algorithmes déjà implémentés avec OMOP.
3. Parler des designs d'études possibles pour utiliser ces algorithmes dans nos hôpitaux à brève échéance.
4. Partager le travail de futurs développements entre les hôpitaux.
5. Parler de l'harmonisation des terminologies en France.
au sein d'InterHop.org, nous continuons le développement de SUSANA, un  logiciel d'alignement terminologique et collaboratif.
6. Parler du développement d'architectures BigData opensource et ubiquitaire pour la santé.
Nous développons au sein d'InterHop une infrastructure BigData facilement déployable (Ansible, GreenPlum, Jupyter...). Voici un lien vers cette plateforme :
 [https://framagit.org/interhop](https://framagit.org/interhop){:target="_blank"
